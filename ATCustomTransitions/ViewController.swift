//
//  ViewController.swift
//  ATCustomTransitions
//
//  Created by Dejan on 19/02/2019.
//  Copyright © 2019 agostini.tech. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailVC = segue.destination as? DetailsViewController {
            detailVC.image = imageView.image
            detailVC.imageTitle = titleLabel.text
        }
    }
    @IBAction func unwindToViewController(_ unwindSegue: UIStoryboardSegue) {
        // Use data from the view controller which initiated the unwind segue
    }
}

