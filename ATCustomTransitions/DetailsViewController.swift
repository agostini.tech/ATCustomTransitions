//
//  DetailsViewController.swift
//  ATCustomTransitions
//
//  Created by Dejan on 19/02/2019.
//  Copyright © 2019 agostini.tech. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    public var image: UIImage?
    public var imageTitle: String?
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageView.image = image
        self.titleLabel.text = imageTitle
    }
}
