//
//  ShowDetailsSegue.swift
//  ATCustomTransitions
//
//  Created by Dejan on 20/02/2019.
//  Copyright © 2019 agostini.tech. All rights reserved.
//

import UIKit

class ShowDetailsSegue: UIStoryboardSegue {
    override func perform() {
        destination.transitioningDelegate = self
        super.perform()
    }
}

extension ShowDetailsSegue: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        return ShowDetailsAnimator()
    }
}
